


require('../lib/mongo');
const API = require('../lib/api');

async function updateWithModel(model) {
    
    let index = 0;
    while(true){

        let accounts = await model.find({}).skip(index * 100).limit(100);

        if(accounts.length == 0) break;

        for(let account of accounts){
            
            await updateBalance(account);
        }
        index++;
    }

}


async function updateBalance(instance) {

    let address = instance.address;

    let data = await API.getBalance(address);
   
    if(isNaN(data.balance) == false){
        instance.balance = data.balance;
        instance.confirmedBalance = data.confirmedBalance;
        instance.stakingBalance = data.stakingBalance;

        instance.realBalance =  instance.confirmedBalance + instance.outLeasesBalance;
        await instance.save();
    }
}


async function update(){

    await updateWithModel(Account);
    process.exit(0);
}


update();