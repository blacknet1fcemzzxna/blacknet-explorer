


require('../lib/mongo');


async function clearAllData() {
    

    await Block.deleteMany({});
    await Account.deleteMany({});
    await Transaction.deleteMany({});

    console.log('completed')
    process.exit(0)
}

setTimeout(clearAllData, 500);