


const Router = require('koa-router');
const router = new Router({
    prefix: '/api'
});
const BigNumber = require('bignumber.js');
const API = require('../lib/api');
const Utils = require('../lib/utils');

const request = require("request-promise")

router.get('/alias', async (ctx, next) => {

    let query = ctx.query;
    let res = { status: false , message: 'The Signature does not match the given Address/Name'}

    if(!Utils.verifyAccount(query.account)){
        res.filed = "account";
    }
    if(!Utils.verifySign(query.signature)){
        res.filed = "signature";
    }
    if(!Utils.verifyAlias(query.display_name)){
        res.filed = "display_name";
    }
    res.status = await API.verifySign(query.account, query.signature, query.display_name)
    if(res.status){
        let account = await Account.findOne({address: query.account})
        if(account){
            account.displayName = query.display_name
            account.save();
        }else{
            res.status = false
            res.message = "account not found"
        }
    }
    ctx.body = res;
});

router.get('/price', async (ctx, next) => {
    // let ex = await Exchange.findOne({coin: "BLN"}).sort({ time: 'desc' });
    // if(ex){
    let ex = await GetEx()
    // }
    ctx.body = {
        lines: ex.lines,
        price: ex.price
    };
});

router.get('/recent_blocks', async (ctx, next) => {

    ctx.body = await Block.find({}).sort({ height: 'desc' }).limit(50);
});

router.get('/recent_transactions', async (ctx, next) => {

    ctx.body = await Transaction.find({ type: {$not: { $gt: 253 }} }).limit(50).sort({ time: 'desc' });
});


router.get('/tx/:hash', async (ctx, next) => {
   
    if(!Utils.verifyHash(ctx.params.hash)){
        return next()
    }

    let hash = ctx.params.hash, query = { txid: hash.toUpperCase() };

    let tx = await Transaction.findOne(query);
    if(!tx){
        tx = global.txpool[hash.toUpperCase()]
    }
    if(!tx){
        return next()
    }
    tx.amountStr = toAmountStr(tx.amount)
    tx.feeStr = toAmountStr(tx.fee)
    tx.typeStr = getTxType(tx.type);


    ctx.body = tx;
});

router.get('/top_accounts', async (ctx, next) => {

    let order = ctx.query.order_by || 'rich';

    if(order == 'rich'){
        query = { realBalance: 'desc' };
    }else if(order == 'pos'){
        query = { blocks: 'desc'};
    }else{
        query = { stakingBalance: 'desc'};
    }
   
    ctx.body = await Account.find({}).limit(100).sort(query);;
});

router.get('/top_accounts_chardata', async (ctx, next) => {

    let order = ctx.query.order_by || 'rich', accounts, chartData=[], supply= overview.supply, params={};

    if(order == 'rich'){
        query = { realBalance: 'desc' };
        params = {
            title: "",
            name: "Balance"
        }
    }else if(order == 'pos'){
        query = { blocks: 'desc'};
        params = {
            title: "",
            name: "Blocks"
        }
        // 计算总量
        let count = await Account.aggregate([{$match: {blocks: { $gt: 0 }}}, {$group : {_id : null, totalBlocks : {$sum : "$blocks"}}}])
        if(count[0] && count[0].totalBlocks) {
            supply = count[0].totalBlocks
        }
    }else{
        query = { stakingBalance: 'desc'};
        params = {
            title: "",
            name: "Balance"
        }
        // 计算总量
        let count = await Account.aggregate([{$match: {stakingBalance: { $gt: 0 }}}, {$group : {_id : null, stakingBalance : {$sum : "$stakingBalance"}}}])
        if(count[0] && count[0].stakingBalance) {
            supply = count[0].stakingBalance
        }
    }
   
    accounts = await Account.find({}).limit(100).sort(query);
    // accounts = accountser(accounts);

    chartData = formartAccountChartData(accounts,supply,order)

    ctx.body = {
        chart: chartData,
        params: params
    };
});


function formartAccountChartData(accounts, supply, type) {
    var chartdata = []
    supply = new BigNumber(supply)
    var total = new BigNumber(0)
    accounts.map(account => {
        switch (type) {
            case "rich":
                var balance = new BigNumber(account.balance).dividedBy(1e8)
                chartdata.push([account.address, balance.toNumber()])
                total = BigNumber.sum.apply(null, [total, balance])
                break;
            case "pos":
                var blocks = new BigNumber(account.blocks)
                chartdata.push([account.address, blocks.toNumber()])
                total = BigNumber.sum.apply(null, [total, blocks])
            default:
                var stakingBalance = new BigNumber(account.stakingBalance)
                chartdata.push([account.address, stakingBalance.toNumber()])
                total = BigNumber.sum.apply(null, [total, stakingBalance])
                break;
        }
    });
    chartdata.push(["Other", parseFloat(supply - total)])
    return chartdata
}


router.get('/block_genesis', async (ctx, next) => {
    let block = {
        size: 34100,
        blockHash: "I see potential to scale better than Ethereum and cheaper than EOS",
        height: 0,
        version: "1",
        previous: "The Blackcoin Project",
        time: "1545555600",
        generator: "000000000000000000000000000000000000000000000000000000000000000",
        contentHash: "000000000000000000000000000000000000000000000000000000000000000",
        signature: new Array(100).join('0'),
        reward: "1,000,000,000",
    };
    block.transactions = await Transaction.find({type: 999});
    block.transactions = block.transactions.sort(function(x,y){
        return y.amount - x.amount;
    });
    ctx.body = block;
});


router.get('/block/:height', async (ctx, next) => {

    let block;
    if(Utils.verifyBlockNumber(ctx.params.height)){
        block = await Block.findOne({ height: ctx.params.height });
    }else if(Utils.verifyHash(ctx.params.height)){
        block = await Block.findOne({ blockHash: String(ctx.params.height).toUpperCase() });
    }else{
        return next()
    }

    if(!block){
        return await ctx.render('404', {msg: "Block not found"})
    }
    let tx = await Transaction.findOne({type: 254, blockHash: block.blockHash});
    let txns = [tx].concat(block.transactions);
    block.transactions = txns;
    ctx.body = block;
});

router.get('/account/ledger/:address', async (ctx, next) => {

    let address = ctx.params.address.toLowerCase()
    let account = await Account.findOne({ address: address });

    if(!account){
        account = {
            balance: 0,
            address,
            confirmedBalance: 0,
            stakingBalance: 0,
            txamount: 0
        }
    }
    ctx.body = account;
});


router.get('/account/:address', async (ctx, next) => {
    let start = Date.now()
    if(!Utils.verifyAccount(ctx.params.address)){
        return next()
    }
    let txns = [], count = 0, page = ctx.query.page || 1, page_number = 0, pager = [];
    let address = ctx.params.address.toLowerCase(), query = { $or: [{ from: address }, { to: address }] };
    let filter_pos_generated = ctx.query.filter_pos_generated || 0;
    let type = ctx.query.type || 'all';
    let account = await Account.findOne({ address: address });
    

    console.log(`#1 time is ${Date.now() - start}ms`)
    if(type != 'all'){
        if(type == "genesis"){
            query.type = 999
        }else{
            query.type = type
        }
    }else{
        if(filter_pos_generated == '1'){
            query.type = {$not: {$eq: 254}};
        }
    }

    if(!account){
        account = {
            balance: 0,
            address,
            confirmedBalance: 0,
            stakingBalance: 0,
            txamount: 0
        }
    }else{
        txns = await Transaction.find(query).skip( (page-1)*100).limit(100).sort({ time: -1 }).lean();
        account.txamount = await Transaction.countDocuments(query);
        account.balancestr = new BigNumber(account.balance).dividedBy(1e8).toFixed(8);
    }

    if(txns.length == 100 || page > 1){
        count = account.txamount;
        page_number = Math.ceil(count / 100);
        pager = getPager(+page, page_number);
    }
    ctx.body = {
        txns,
        account,
        page_number,
        filter_pos_generated, pager, page, type
    }
});

router.get('/pool/:address', async (ctx, next) => {
    
    let page = ctx.query.page, type = ctx.query.type;
    let address = ctx.params.address.toLowerCase(), query = { $or: [{ from: address }, { to: address }]};

    if(type){
        query.type = type;
    }

    ctx.body = await Transaction.find(query).skip( (page-1)*100).limit(100).sort({ time: -1 }).lean();;
});

router.get('/overview', async (ctx, next) => {

    ctx.body = global.overview;
});

module.exports = router;

/**
 * format amount to str 
 * @method toAmountStr
 * @param {number} amount
 * @return {string} str
 */
function toAmountStr(amount) {
    return new BigNumber(amount).dividedBy(1e8).toFixed(8);
}


var txType = ["Transfer", "Burn", "Lease", "CancelLease", "Bundle", "CreateHTLC",
    "UnlockHTLC", "RefundHTLC", "SpendHTLC", "CreateMultisig", "SpendMultisig"];

    txType[254] = "PoS Generated";
    txType["all"] = "All";

function getTxType(type){

    return txType[type] || 'genesis';
}

function getPager(now, max){
    
    let arr = [now -3, now -2, now -1, now, now + 1, now + 2, now + 3];

    arr = arr.filter((page)=>{

        if( page < 1 ) return false;

        if( page > max ) return false;

        return true;
    });

    return arr;
}


async function GetEx(){
    let ex = await Exchange.findOne({coin: "BLN"}).sort({ time: 'desc' });
    if(!ex){
        ex = new Exchange({coin: "BLN",lines:[],price:{}})
    }
    let body = await new Promise((resolve) => {
        request({
            url: "https://www.citex.co.kr/quot/queryCandlestick?symbol=81&range=86400000",
            headers: {
                'User-Agent': 'Mozilla/5.0 (Linux; Android 8.0; Pixel 2 Build/OPD3.170816.012) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Mobile Safari/537.36',
                "Referer": "https://www.citex.co.kr/"
            },
            forever: true,
            json: true
        }, (err, res, body)=>{
            resolve(body);
        });
    });
    if(body && body.success && body.data.lines.length){
        let lines = []
        body.data.lines.map((line)=>{
            lines.push([line[0], line[4]])
        })
        if(lines.length){
            ex.lines = lines
        }
    }
    body = await new Promise((resolve) => {
        request({
            url: "https://www.citex.co.kr/quot/queryIndicatorList",
            headers: {
                'User-Agent': 'Mozilla/5.0 (Linux; Android 8.0; Pixel 2 Build/OPD3.170816.012) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Mobile Safari/537.36',
                "Referer": "https://www.citex.co.kr/"
            },
            forever: true,
            json: true
        }, (err, res, body)=>{
            resolve(body);
        });
    });
    if(body && body.length){
        for (const key in body) {
            if (body.hasOwnProperty(key)) {
                if(body[key]["contractId"]==81){
                    ex.price[body[key].symbol] = {
                        "price": body[key].lastPrice,
                        "percent": new BigNumber(body[key].priceChangeRadio).times(1e2).toFixed(2)
                    }
                    if(body[key].contractVos.length){
                        body[key].contractVos.map((p)=>{
                            ex.price[p.symbol] = {
                                "price": p.lastPrice,
                                "percent": new BigNumber(p.priceChangeRadio).times(1e2).toFixed(2)
                            }
                        })
                    }
                    break;
                }
            }
        }
    }
    console.log(ex)
    ex.save();
    return ex
}