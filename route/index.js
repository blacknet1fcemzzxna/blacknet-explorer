


const Router = require('koa-router');
const router = new Router();
const API = require('../lib/api');
const Utils = require('../lib/utils');

router.get('/', async (ctx, next) => {

    await ctx.render('recent_blocks', { recent: true , i18n: ctx.i18n});
});

router.get('/alias', async (ctx, next) => {
    await ctx.render('alias', { i18n: ctx.i18n });
});
router.get('/alias/:address', async (ctx, next) => {
    await ctx.render('alias', { address: ctx.params.address ,  i18n: ctx.i18n });
});

router.get('/:keyword', async (ctx, next) => {

    let keyword = ctx.params.keyword;

    if (/\d+/.test(keyword)) {
        await ctx.render('block', { height: keyword , i18n: ctx.i18n});
    }

    if (['about', 'recent_blocks', 'recent_transactions', 'top_accounts', 'dashboard'].indexOf(keyword) !== -1) {

        let ret = { i18n: ctx.i18n };
        ret[keyword] = true;

        await ctx.render(keyword, ret);
    }

    if (keyword.length == 67 && /^blacknet[a-zA-Z0-9]{59}$/.test(keyword)) {
        let address = keyword;
        let type = ctx.query.type || 'all';
        await ctx.render('account', { address, type ,  i18n: ctx.i18n });
    }

    if (keyword.length == 64) {

        let instance = await Block.findOne({ blockHash: keyword.toUpperCase() });
        if (instance) {
            await ctx.render('block', { height: instance.height ,  i18n: ctx.i18n });
        } else {
            instance = await Transaction.findOne({ txid: keyword.toUpperCase() });
            await ctx.render('tx', { i18n: ctx.i18n });
        }
    }
    await next();
});

router.get('/api', async (ctx, next) => {


    await ctx.render('apis', { apis: true , i18n: ctx.i18n });
});


router.get('/recent_blocks', async (ctx, next) => {


    await ctx.render('recent_blocks', { recent: true , i18n: ctx.i18n});
});

router.get('/recent_transactions', async (ctx, next) => {

    await ctx.render('recent_transactions', { recent_transactions: true , i18n: ctx.i18n});
});


router.get('/tx/:hash', async (ctx, next) => {

    await ctx.render('tx', {i18n: ctx.i18n});
});

router.get('/search', async (ctx, next) => {
    let key = ctx.query.q;
    if (Utils.verifyAccount(key)) {
        return ctx.redirect('/' + key);
    }
    if (Utils.verifyBlockNumber(key)) {
        return ctx.redirect('/' + key);
    }
    if (Utils.verifyHash(key)) {
        let block = await Block.findOne({ blockHash: key.toUpperCase() });
        if (block) {
            return ctx.redirect('/block/' + key);
        }
        let tx = await Transaction.findOne({ txid: key.toUpperCase() });
        if (tx) {
            return ctx.redirect('/tx/' + key);
        }
    }
    next()
});


router.get('/top_accounts', async (ctx, next) => {
    let order = ctx.query.order_by || 'rich';

    await ctx.render('top_accounts', { top_accounts: true, order, i18n: ctx.i18n });
});

router.get('/block/:height', async (ctx, next) => {

    let height = ctx.params.height;
    await ctx.render('block', { height , i18n: ctx.i18n});
});

router.get('/dashboard', async (ctx, next) => {


    let peers = await Peer.find({});
    let currentPeers = await API.getPeerInfo();


    for (var cpeer of currentPeers) {

        let index = cpeer.remoteAddress.lastIndexOf(':');
        let ip = cpeer.remoteAddress.slice(0, index);
        let dpeer = await Peer.findOne({ ip });

        cpeer.ip = ip;
        cpeer.port = cpeer.remoteAddress.slice(index + 1);
        if (dpeer) {
            cpeer.location = dpeer.location;
        } else {
            cpeer.location = {};
        }

    }

    await ctx.render('dashboard', { currentPeers, peers, dashboard: true , i18n: ctx.i18n});
});

router.get('/account/:address', async (ctx, next) => {
    let address = ctx.params.address;
    let type = ctx.query.type || 'all';
    await ctx.render('account', { address, type , i18n: ctx.i18n});
});


module.exports = router;

