var Utils = {
    verifyAccount: (account)=>{
        if(/^blacknet[a-z0-9]{59}$/i.test(account)){
            return true
        }
        return false
    },
    verifyHash: (hash)=>{
        if(/(\w){64}/i.test(String(hash))){
            return true
        }
        return false
    },
    verifySign: (sign)=>{
        if(Object.prototype.toString.call(sign) === "[object String]" && sign.length === 128){
            return true
        }
        return false
    },
    verifyAlias: (alias)=>{
        if(Object.prototype.toString.call(alias) === "[object String]"){
            return true
        }
        return false
    }
}

Blacknet.controller('aliasController', ['$scope', '$http', '$timeout', function ($scope, $http, $timeout) {

    if(window.location && window.location.pathname){
        var ps = window.location.pathname.split("/");
        if(ps[1]){
            $scope.account = ps[2]
        }
    }

    $scope.res_msg = ""

    $scope.claimClick = function claimClick() {
        
        
        if(!Utils.verifyAlias($scope.display_name)){
            document.querySelector("#display_name").focus()
            return 
        }
        if(!Utils.verifySign($scope.signature)){
            document.querySelector("#signature").focus()
            return 
        }
        $scope.res_msg = "";
        $http.get(`/api/alias?account=${$scope.account}&signature=${$scope.signature}&display_name=${$scope.display_name}`).then(function (res) {
            if(res.status == 200 && res.data.status){
                $timeout(function () {
                    window.location.href="/account/"+ $scope.account
                }, 1000 * 1);
            }else{
                $scope.res_msg = res.data.message
            }
        })
    }

}]);

Blacknet.controller('recent_blocks', ['$scope', '$http', '$timeout', function ($scope, $http, $timeout) {

    $scope.blocks = [];
    function update(){
        $http.get('/api/recent_blocks').then(function (res) {
            $scope.blocks = res.data;
        });
        $timeout(update, 1000 * 30);
    }
    update();

}]);

Blacknet.controller('recent_transactions', ['$scope', '$http', '$timeout',function ($scope, $http, $timeout) {

    $scope.transactions = [];
    function update(){
        $http.get('/api/recent_transactions').then(function (res) {
            $scope.transactions = res.data;
        })
        $timeout(update, 1000 * 30);
    }
    update();
}]);

Blacknet.controller('top_accounts', ['$scope', '$http', function ($scope, $http) {
    $scope.top_accounts = [];
    $http.get('/api/top_accounts' + location.search).then(function (res) {
        $scope.top_accounts = res.data;
    });
    $http.get('/api/top_accounts_chardata' + location.search).then(function (res) {
        var params = {
            title: res.data.params.title,
            name: res.data.params.name
        }
        var chart = {
            backgroundColor: "#3B3B3B",
            plotBackgroundColor: "#3B3B3B",
            plotBorderWidth: null,
            plotShadow: false
        };
        var title = {
            text: params.title,
            style: { "color": "#e8ba3f", "fontSize": "18px" }
        };      
        var tooltip = {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        };
        var plotOptions = {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || '#adadad'
                    }
                }
            }
        };
        var series= [{
            type: 'pie',
            name: params.name,
            data: res.data.chart}]; 
        var json = {
            credits:{
                enabled: false
            }
        };   
        json.chart = chart; 
        json.title = title;     
        json.tooltip = tooltip;  
        json.series = series;
        json.plotOptions = plotOptions;
        $('.chart').highcharts(json);  
    });
}]);

Blacknet.controller('transaction', ['$scope', '$http', function ($scope, $http) {
    $scope.tx = {
        amount: 0,
        fee: 0
    };
    let url = '/api' + location.pathname;

    if(location.pathname.indexOf('/tx/') === -1){
        url = '/api/tx' + location.pathname;
    }

    $http.get(url).then(function (res) {
        $scope.tx = res.data;
    });
}]);

Blacknet.controller('blockController', ['$scope', '$http', function ($scope, $http) {
    $scope.block = {};
    let url = '/api' + location.pathname;

    if(location.pathname.indexOf('/block/') === -1){
        url = '/api/block' + location.pathname;
    }

    let height = document.querySelector('#block_detail').getAttribute('data-height');
   
    if(height == 0){
        url = '/api/block_genesis';
    }
    
    $http.get(url).then(function (res) {
        res.data.transactions = res.data.transactions.map(function(tx){
            if(typeof tx.data == 'string') tx.data = JSON.parse(tx.data);
            return tx;
        });
        $scope.block = res.data;
    });
}]);

Blacknet.controller('account', ['$scope', '$http', function ($scope, $http) {
    $scope.account = {
        balance: 0,
        confirmedBalance: 0,
        stakingBalance: 0
    };
    $scope.txns = [];

    $scope.types = ['all', 0,"genesis",2,3, 254];

    $scope.pathname = location.pathname;
    $scope.isloaded = false;

    let url = '/api' + location.pathname;

    if(location.pathname.indexOf('/account/') === -1){
        url = '/api/account' + location.pathname;
    }

    $http.get(url + location.search).then(function (res) {
        $scope.account = res.data.account;
        $scope.leaseAndCancel = res.data.leaseAndCancel;
        $scope.txns = res.data.txns.map(function(tx){
            if(typeof tx.data == 'string') tx.data = JSON.parse(tx.data);
            return tx;
        });
        $scope.page = res.data.page;
        $scope.pager = res.data.pager;
        $scope.page_number = res.data.page_number;
        $scope.filter_pos_generated = res.data.filter_pos_generated == 1 ? true : false;
        $scope.isloaded = true;
    });

    $scope.changeUrl = function () {

        location.href = location.pathname + '?page=1&filter_pos_generated=' + ($scope.filter_pos_generated ? 1 : 0);
    }
}]);
Blacknet.controller('bodyController', ['$scope', '$http', '$timeout', function ($scope, $http, $timeout) {

    $scope.overview = {};
    
    function toggleNav() {
        let display = document.querySelector('.nav').style.display;

        display = display != 'block' ? 'block': 'none';
        document.querySelector('.nav').style.display = display;
    }

    document.querySelector('.touch-menu button').addEventListener('touchstart', toggleNav ,false);

    function fetchOverview(){
        $http.get('/api/overview').then(function(res){
            $scope.overview = res.data;
            $timeout(fetchOverview, 30 * 1000);
        });
    }
    fetchOverview();
}]);


Blacknet.controller('searchController', ['$scope', '$http', function ($scope, $http) {

    $scope.search = function(){
        if($scope.searchText)
            location.href = '/search?q=' + $scope.searchText;
    };
}]);


Blacknet.controller('aboutController', ['$scope', '$http', function ($scope, $http) {

    $scope.Supply = Supply;

    $http.get('/api/price' + location.search).then(function (res) {
        $scope.price = res.data.price;
        if($scope.price["BLN-BTC"] && parseFloat($scope.price["BLN-BTC"].percent) < 0){
            $scope.percentColor = "red"
        }else{
            $scope.percentColor = "green"
        }
        Highcharts.chart('chart-trade', {
            credits:{
                enabled: false
            },
            chart: {
                zoomType: 'x',
                backgroundColor: "#3B3B3B",
                plotBackgroundColor: "#3B3B3B",
                borderWidth:0
            },
            title: {
                text: '',
                enabled: false
            },
            subtitle: {
                enabled: false
            },
            xAxis: {
                type: 'datetime',
                dateTimeLabelFormats: {
                    millisecond: '%H:%M:%S.%L',
                    second: '%H:%M:%S',
                    minute: '%H:%M',
                    hour: '%H:%M',
                    day: '%m-%d',
                    week: '%m-%d',
                    month: '%Y-%m',
                    year: '%Y'
                },
                visible: false
            },
            tooltip: {
                dateTimeLabelFormats: {
                    millisecond: '%H:%M:%S.%L',
                    second: '%H:%M:%S',
                    minute: '%H:%M',
                    hour: '%H:%M',
                    day: '%Y-%m-%d',
                    week: '%m-%d',
                    month: '%Y-%m',
                    year: '%Y'
                },
                enabled: true,
                pointFormatter: function() {
                    return '<b>'+ this.series.name +'</b>: '+ parseFloat(this.y).toFixed(8) + " BTC";
                }
            },
            yAxis: {
                title: {
                    enabled: false
                },
                visible: false
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                area: {
                    fillColor: {
                        linearGradient: {
                            x1: 0,
                            y1: 0,
                            x2: 0,
                            y2: 1
                        },
                        stops: [
                            [0, Highcharts.getOptions().colors[0]],
                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                        ]
                    },
                    marker: {
                        radius: 2
                    },
                    lineWidth: 1,
                    states: {
                        hover: {
                            lineWidth: 1
                        }
                    },
                    threshold: null
                }
            },
            series: [{
                type: 'area',
                name: 'BLN/BTC',
                data: res.data.lines
            }]
        });
    });
}]);