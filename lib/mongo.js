
var mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/test', {
    useNewUrlParser: true,
    auto_reconnect: true,
    reconnectTries: Number.MAX_VALUE, // Never stop trying to reconnect
    reconnectInterval: 500,
    poolSize: 10
});

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    // we're connected!
    console.log("we're connected!")
});

var ExchangeSchema = new mongoose.Schema({
    price: Object,
    lines: Array,
    time: Number,
    coin: String
});

var BlockSchema = new mongoose.Schema({
    size: Number,
    version: String,
    previous: String,
    time: String,
    generator: String,
    contentHash: String,
    signature: String,
    transactions: Array,
    height: Number,
    blockHash: String,
    reward: String
});

var TransactionSchema = new mongoose.Schema({
    txid: String,
    size: Number,
    signature: String,
    from: String,
    seq: Number,
    blockHash: String,
    fee: Number,
    type: Number,
    data: Object,
    to: String,
    time: String,
    amount: Number,
    blockHeight: Number
});


var AccountSchema = new mongoose.Schema({
    address: String,
    balance: { type: Number, default: 0 },
    txamount: { type: Number, default: 0 },
    confirmedBalance: { type: Number, default: 0 },
    stakingBalance: { type: Number, default: 0 },
    seq: Number,
    blocks: { type: Number, default: 0 },
    realBalance: { type: Number, default: 0 },
    outLeasesBalance: { type: Number, default: 0 },
    inLeasesBalance: { type: Number, default: 0 },
    displayName: { type: String, default: null }
});

var PeerSchema = new mongoose.Schema({
    ip: String,
    port: String,
    location: Object
});

global.Peer = mongoose.model('Peer', PeerSchema);
global.Block = mongoose.model('Block', BlockSchema);
global.Account = mongoose.model('Account', AccountSchema);
global.Transaction = mongoose.model('Transaction', TransactionSchema);
global.Exchange = mongoose.model('Exchange', ExchangeSchema);