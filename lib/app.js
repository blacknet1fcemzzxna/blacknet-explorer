
global.config = require('../config');

const API = require('./api');
const Bignumber = require('bignumber.js');
const IPGeolocationAPI = require('ip-geolocation-api-javascript-sdk');
const ipgeolocationApi = new IPGeolocationAPI(config.ipgeolocation_key, false);
var GeolocationParams = require('ip-geolocation-api-javascript-sdk/GeolocationParams.js');

setInterval(initOverview, 60 * 1000);

async function initOverview() {
    let ledger = await API.getInfo();
    let nodeInfo = await API.getNodeInfo();
    ledger.supply = new Bignumber(ledger.supply).dividedBy(1e8).toFixed(0);
    global.overview = {
        ...ledger,
        ...nodeInfo
    };
}
module.exports = initOverview;


global.txpool = {};
async function getTxPool(){

    let data = await API.getTxPool();
    let cache = {};

    if(data.tx.length == 0) return;
    for(let txhash of data.tx){

        let tx = await API.getTxWithTxHash(txhash);
        tx.amount = tx.data.amount;
        tx.to = tx.data.to;
        tx.txpool = true;
        tx.txid = tx.hash;
        cache[txhash] = tx;
    }

    global.txpool = cache;
}
getTxPool();
setInterval(getTxPool, 10 * 1000);





initOverview();


setInterval(ipLocation, 1000 * 60 * 60);
ipLocation();
async function ipLocation() {

    let peerdb = await API.getPeerDB();
    let peers = peerdb.peers;
    if(!peers)  return;

    for (let peer of peers) {

        let index = peer.lastIndexOf(':'), itslimit;
        let ip = peer.slice(0, index).replace(/[\[\]]/g, '');

        let p = await Peer.findOne({ ip });
        if(p){
            itslimit = p.location.message == 
            "You have exceeded your subscription's requests limit of 1000 per day.";
        }
        if (p && !itslimit) continue;

        if (!p) {
            p = { ip, port: peer.slice(index + 1) };
        }

        p.location = await getLocation(ip);

        if (!itslimit) {
            p = new Peer(p);
        }
        await p.save();
    }
}

function getLocation(ip) {

    return new Promise((resolve, reject) => {
        var geolocationParams = new GeolocationParams();
        geolocationParams.setIPAddress(ip);
        geolocationParams.setFields("geo,time_zone,currency");
        ipgeolocationApi.getGeolocation(function (json) {
            resolve(json);
        }, geolocationParams);
    })
}
