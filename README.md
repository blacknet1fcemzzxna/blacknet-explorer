# blacknet-explorer

Explorer for [blacknet](https://blacknet.ninja/)

website: [https://www.blnscan.io/](https://blnscan.io/)

# Get Started ( Ubuntu )

## 0 env ready
```
apt-get install -y unzip wget curl git default-jre
```
## 1. install mongodb
```
wget https://fastdl.mongodb.org/linux/mongodb-linux-x86_64-4.0.11.tgz
tar zvxf mongodb-linux-x86_64-4.0.11.tgz
cd mongodb-linux-x86_64-4.0.11
cp -r * /usr/local
mkdir -p /data/db
nohup mongod --dbpath=/data/db --bind_ip=127.0.0.1 &

```
## 1.1 create mongo Index
```

mongo

db.transactions.createIndex({time:1, type:1})
db.blocks.createIndex({time:1, height:1})
db.account.createIndex({time:1, balance:1,realBalance:1,stakingBalance:1, blocks:1})

```


## 2. install nodejs
```
wget https://nodejs.org/dist/v10.16.0/node-v10.16.0-linux-x64.tar.xz
tar -vxf node-v10.16.0-linux-x64.tar.xz
cd node-v10.16.0-linux-x64
cp -r * /usr/local
```

## 3. install Blacknet
```
wget https://vasin.nl/blacknet-0.2.3.zip
unzip blacknet-0.2.3.zip
cd blacknet-0.2.3/bin/
nohup ./blacknet &
```

## 4. clone explorer source
```
git clone https://gitlab.com/blacknet1fcemzzxna/blacknet-explorer
cd blacknet-explorer
npm i
npm install pm2 -g 

pm2 start init.js
pm2 start app.js
```

## 5. open http://your_server_ip:3000/


## 6. crontab ( for update balance ) Required
```
*/3 * * * * cd YourDIR/blacknet-explorer/ && /usr/local/bin/node tools/update.js >> /var/log/crontab.log
```
